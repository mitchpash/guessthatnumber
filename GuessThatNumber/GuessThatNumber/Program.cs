﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace GuessThatNumber
{
    public class Program
    {
        //set debugging option
        private static bool debugging = true;

        //Init global variables
        private static Random random = new Random();

        private static int NumberToGuess = 0;

        private static void Main(string[] args)
        {
            //track how much further the user needs to go
            int howFarOff = 0;
            //track number of guesses
            int numberOfGuesses = 0;
            //set keep guessing option (false when user guesses number properlly)
            bool keepGuessing = true;
            //Assign a new number to guess
            NumberToGuess = random.Next(1, 101);

            while (keepGuessing)
            {
                //Get user input
                Console.Write("Pick a number between 1 and 100: ");
                //Convert to an int
                string usersNumber = Console.ReadLine();

                //Use a validator to make sure the user's input is a number between 1 and 100
                if (ValidateInput(usersNumber))
                {
                    numberOfGuesses++;

                    //Check if the user's number is the one
                    if (int.Parse(usersNumber) == NumberToGuess)
                    {
                        Console.WriteLine("That's it! Eureka!");
                        Console.WriteLine("It took you {0} guesses", numberOfGuesses);
                        keepGuessing = false;
                    }
                    else if (IsGuessTooHigh(int.Parse(usersNumber)))
                    {
                        howFarOff = int.Parse(usersNumber) - NumberToGuess;
                        if (howFarOff >= 50)
                        {
                            Console.WriteLine("That guess is just too damn high...\n");
                        }
                        else if (howFarOff >= 20)
                        {
                            Console.WriteLine("Eh you're kind of close... lower man...\n");
                        }
                        else if (howFarOff >= 10)
                        {
                            Console.WriteLine("You're just a little to high...\n");
                        }
                        else
                        {
                            Console.WriteLine("A little lower and you're there!\n");
                        }
                    }
                    else if (IsGuessTooLow(int.Parse(usersNumber)))
                    {
                        howFarOff = NumberToGuess - int.Parse(usersNumber);
                        if (howFarOff >= 50)
                        {
                            Console.WriteLine("That guess is just too damn low...\n");
                        }
                        else if (howFarOff >= 20)
                        {
                            Console.WriteLine("Eh you're kind of close... higher man...\n");
                        }
                        else if (howFarOff >= 10)
                        {
                            Console.WriteLine("You're just a little to low...\n");
                        }
                        else
                        {
                            Console.WriteLine("A little higher and you're there!\n");
                        }
                    }
                }
                else
                {
                }
            }

            //End of Program
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        public static bool ValidateInput(string userInput)
        {
            //check to make sure the user's input is a number
            int n;
            bool isNumeric = int.TryParse(userInput, out n);


            if (isNumeric)
            {
                //check to make sure that the users input is a valid number between 1 and 100.
                if (n > 0 && n < 101)
                    return true;
                else
                    Console.WriteLine("What part about pick a number between 1 and 100 wasn't clear enough?\n");
            }
            else
            {
                Console.WriteLine("That isn't even a number... come on\n");
            }
            
            return false;
        }

        public static void SetNumberToGuess(int number)
        {
            //Override global variable holding the number the user needs to guess.
            NumberToGuess = number;
        }

        public static bool IsGuessTooHigh(int userGuess)
        {
            //return true if the number guessed by the user is too high
            if (userGuess > NumberToGuess)
                return true;
            return false;
        }

        public static bool IsGuessTooLow(int userGuess)
        {
            //return true if the number guessed by the user is too low
            if (userGuess < NumberToGuess)
                return true;
            return false;
        }

    }

}